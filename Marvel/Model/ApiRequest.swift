//
//  ApiRequest.swift
//  Marvel
//
//  Created by Sérgio Toledo on 07/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import Foundation

public protocol ApiRequest: Encodable {
    
    associatedtype Response: Decodable
    
//    var endpointPath: String {
//        get
//    }
    
}
