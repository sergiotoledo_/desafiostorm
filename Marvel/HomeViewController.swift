//
//  ViewController.swift
//  Marvel
//
//  Created by Sérgio Toledo on 06/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//
import Foundation
import UIKit
import CryptoSwift
import RappleProgressHUD


class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{

    @IBOutlet weak var CollectionView: UICollectionView!
    var characters: [[String:Any]] = [[String:Any]]()
    
    var offset: Int = 0
    var total: Int = 0
    
    @IBAction func searchCharacteres(_ sender: Any) {
        self.performSegue(withIdentifier: "search", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.CollectionView.delegate = self
        self.CollectionView.dataSource = self
        
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 270, height: 30))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 270, height: 30))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
        
        loadCharacters(offset: self.offset, completionHandler: { result in
            
            if result {
                DispatchQueue.main.async { [weak self] in
                    self!.CollectionView.reloadData()
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
            
        })
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return characters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! HomeCollectionViewCell
        
        if characters.isEmpty == false {
            cell.characterImage.tag = indexPath.row
            cell.characterImage.loadImageUsingCache(withUrl: (characters[indexPath.row]["thumbnail"] as! String))
            cell.characterName.text = (characters[indexPath.row]["name"] as! String)
        }
        
        if (indexPath.row == characters.count - 1 && self.total > characters.count) {
            loadCharacters(offset: self.offset, completionHandler: { result in
                
                if result {
                    DispatchQueue.main.async { [weak self] in
                        self!.CollectionView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                }
                
            })
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = 250
        return CGSize(width: collectionView.bounds.size.width, height: CGFloat(height))
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "details", sender: characters[indexPath.row])
        
    }
    
    func loadCharacters(offset: Int, completionHandler: @escaping (_ result: Bool) -> Void) {
        RappleActivityIndicatorView.startAnimating()
        let apiClient = ApiMarvel(publicKey: Constants.publicKey,
                                  privateKey: Constants.privateKey,
                                  url: "characters")
        
        apiClient.send(GetCharacters(limit: Constants.limitPaginationHome, offset: offset)) { response in
            
            switch response {
            case .success(let dataContainer):
                self.offset = dataContainer.limit + dataContainer.offset
                self.total = dataContainer.total
                
                for character in dataContainer.results {
                    self.characters.append(["id" : character.id, "thumbnail" : (character.thumbnail?.url.absoluteString)!, "name" : character.name!, "description" : character.description!])
                }
                completionHandler(true)
                

            case .failure(let error):
                print(error)
                completionHandler(false)
            }
            
            
        }
        
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "details" {
            let detailsViewController = segue.destination as! DetailsViewController
            let details = sender as? [String:Any]
            detailsViewController.details = details!
        }
        
    }
    
}

