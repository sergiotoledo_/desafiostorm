//
//  ComicsViewController.swift
//  Marvel
//
//  Created by Sérgio Toledo on 11/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//
import Foundation
import UIKit
import RappleProgressHUD

class ComicsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var details : [String:Any] = [String:Any]()
    var comicsList: [[String:Any]] = [[String:Any]]()
    @IBOutlet weak var pagination: UILabel?
    
    var offset: Int = 0
    var total: Int = 0

    @IBOutlet weak var comicTitle: UILabel?
    @IBOutlet weak var comicsCollection: UICollectionView!
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        comicsCollection.delegate = self
        comicsCollection.dataSource = self
        
        loadComics(id: (details["id"] as! Int), offset: self.offset, completionHandler: { result in
            if result {
                DispatchQueue.main.async { [weak self] in
                    self!.comicsCollection.reloadData()
                    RappleActivityIndicatorView.stopAnimation()
                }
            }
        })
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return comicsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "comicCollectionCell", for: indexPath) as! ComicsCollectionViewCell
        
        if comicsList.isEmpty == false {
            cell.comicImage.tag = indexPath.row
            cell.comicImage.loadImageUsingCache(withUrl: (comicsList[indexPath.row]["thumbnail"] as! String))
        }
        
        if (indexPath.row == comicsList.count - 1 && self.total > comicsList.count) {
            loadComics(id: (details["id"] as! Int), offset: self.offset, completionHandler: { result in
                
                if result {
                    DispatchQueue.main.async { [weak self] in
                        self!.comicsCollection.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                }
                
            })
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let visibleIndex = Int(targetContentOffset.pointee.x / comicsCollection.frame.width)
        self.pagination?.text = "\(visibleIndex+1)/\(self.total)"
        self.comicTitle?.text = (comicsList[visibleIndex]["title"] as! String)
        
    }
    
    func loadComics(id: Int, offset: Int, completionHandler: @escaping (_ result: Bool) -> Void){
        RappleActivityIndicatorView.startAnimatingWithLabel("Loading...")
        
        let apiClient = ApiMarvel(publicKey: Constants.publicKey,
                                  privateKey: Constants.privateKey,
                                  url: "characters/\(id)/comics")
        
        apiClient.send(GetComics(limit: Constants.limitPaginationComics, offset: offset)) { response in

            switch response {
            case .success(let dataContainer):
                
                self.total = dataContainer.total
                self.offset = dataContainer.limit + dataContainer.offset

                if (self.comicsList.count == 0 && dataContainer.results.count > 0) {
                    DispatchQueue.main.async { [weak self] in
                        self!.comicTitle?.text = dataContainer.results[0].title
                        self!.pagination?.text = "1/\(dataContainer.total)"
                    }
                }
                else if (dataContainer.results.isEmpty) {
                    let alertController = UIAlertController(title: "Information", message: "No Comics found.", preferredStyle: UIAlertController.Style.alert)
                    let ok = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                        self.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(ok)
                    self.present(alertController, animated: true, completion: nil)
                }
                
                for comic in dataContainer.results {
  
                    self.comicsList.append(["id" : comic.id, "thumbnail" : (comic.thumbnail?.url.absoluteString)!, "title" : comic.title!, "description" : comic.description ?? ""])
                }
                completionHandler(true)

            case .failure(let error):
                print(error)
                completionHandler(false)
            }
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
