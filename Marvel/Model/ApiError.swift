//
//  ApiError.swift
//  Marvel
//
//  Created by Sérgio Toledo on 07/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import Foundation

public enum MarvelError: Error {
    case encoding
    case decoding
    case server(message: String)
}
