//
//  URLQueryEncoder.swift
//  Marvel
//
//  Created by Sérgio Toledo on 07/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import Foundation

enum URLQueryEncoder {
    static func encode<T: Encodable>(_ encodable: T) throws -> [URLQueryItem] {
        
        let parametersData = try JSONEncoder().encode(encodable)
        let parameters = try JSONDecoder().decode([String: HTTPParameter].self, from: parametersData)
        return parameters.map { URLQueryItem(name: $0, value: $1.description) }
    }
}
