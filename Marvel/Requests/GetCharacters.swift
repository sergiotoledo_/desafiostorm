//
//  GetCharacters.swift
//  Marvel
//
//  Created by Sérgio Toledo on 07/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import Foundation

public struct GetCharacters: ApiRequest {
    
    public typealias Response = [ComicCharacter]
    
    public let name: String?
    public let nameStartsWith: String?
    public let limit: Int?
    public let offset: Int?
    
    public init(name: String? = nil,
                nameStartsWith: String? = nil,
                limit: Int? = nil,
                offset: Int? = nil) {
        self.name = name
        self.nameStartsWith = nameStartsWith
        self.limit = limit
        self.offset = offset
    }
}
