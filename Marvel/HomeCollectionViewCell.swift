//
//  CollectionViewCell.swift
//  Marvel
//
//  Created by Sérgio Toledo on 07/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var characterName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //characterName.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundTitle")!)

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        characterImage.image = nil
        
    }
    
}
