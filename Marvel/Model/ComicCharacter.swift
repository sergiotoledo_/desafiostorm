//
//  ComicCharacter.swift
//  Marvel
//
//  Created by Sérgio Toledo on 07/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import Foundation

public struct ComicCharacter: Decodable {
    public let id: Int
    public let name: String?
    public let description: String?
    public let thumbnail: Image?
}
