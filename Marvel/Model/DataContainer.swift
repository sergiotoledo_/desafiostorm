//
//  DataContainer.swift
//  Marvel
//
//  Created by Sérgio Toledo on 07/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import Foundation

/// All successful responses return this, and contains all
/// the metainformation about the returned chunk.
public struct DataContainer<Results: Decodable>: Decodable {
    public let offset: Int
    public let limit: Int
    public let total: Int
    public let count: Int
    public let results: Results
}
