//
//  Constants.swift
//  Marvel
//
//  Created by Sérgio Toledo on 12/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import Foundation

struct Constants {
    
    static let baseEndpointUrl: String = "https://gateway.marvel.com:443/v1/public/"
    static let publicKey: String = "27ba2fb95d1c28e051f30aa2e8a712e9"
    static let privateKey: String = "785d30b82282fadaebf42c7bb5e6944850b3173e"
    static let limitPaginationHome: Int = 6
    static let limitPaginationSearch: Int = 10
    static let limitPaginationComics: Int = 10
    
}
