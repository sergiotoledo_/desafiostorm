//
//  ComicsCollectionViewCell.swift
//  Marvel
//
//  Created by Sérgio Toledo on 11/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import UIKit

class ComicsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var comicImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
}
