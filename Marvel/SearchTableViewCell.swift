//
//  SearchTableViewCell.swift
//  Marvel
//
//  Created by Sérgio Toledo on 11/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var characterThumb: UIImageView!
    @IBOutlet weak var characterTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
