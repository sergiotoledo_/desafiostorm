//
//  GetComics.swift
//  Marvel
//
//  Created by Sérgio Toledo on 10/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import Foundation

public struct GetComics: ApiRequest {
    // When encountered with this kind of enums, it will spit out the raw value
    public enum ComicFormat: String, Encodable {
        case comic = "comics"
        case digital = "digital comic"
        case hardcover = "hardcover"
    }
    
    public typealias Response = [Comic]
    
    public let limit: Int?
    public let offset: Int?
    
    public init(limit: Int? = nil,
                offset: Int? = nil) {

        self.limit = limit
        self.offset = offset
    }
    
}
