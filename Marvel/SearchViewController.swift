//
//  SearchViewController.swift
//  Marvel
//
//  Created by Sérgio Toledo on 10/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import UIKit
import Foundation
import RappleProgressHUD

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var searchTableView: UITableView!
    
    var characters: [[String:Any]] = [[String:Any]]()
    var offset: Int = 0
    var total: Int = 0
    var termSearch: String = ""
    typealias Handler = () -> Void
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        searchTableView.delegate = self
        searchTableView.dataSource = self
        
        search.delegate = self
        search.becomeFirstResponder()
        
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: true, completion: nil)
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.offset = 0
        self.total = 0
        self.characters.removeAll()
        self.termSearch = searchBar.text!
        if searchBar.text != "" {
            searchCharacterByName(name: termSearch, offset: self.offset, completionHandler: { result in
                if result {
                    DispatchQueue.main.async { [weak self] in
                        self!.searchTableView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                }
            })
            searchBar.resignFirstResponder()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellResult", for: indexPath) as! SearchTableViewCell
        
        cell.characterThumb.loadImageUsingCache(withUrl: (characters[indexPath.row]["thumbnail"] as! String))
        cell.characterTitle.text = (characters[indexPath.row]["name"] as! String)
        
        if (indexPath.row == characters.count - 1 && self.total > characters.count) {
            searchCharacterByName(name: self.termSearch, offset: self.offset, completionHandler: { result in
                
                if result {
                    DispatchQueue.main.async { [weak self] in
                        self!.searchTableView.reloadData()
                        RappleActivityIndicatorView.stopAnimation()
                    }
                }
                
            })
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        weak var pvc:UIViewController! = self.presentingViewController?.children[0]
        dismiss(animated: true)
        {
            pvc.performSegue(withIdentifier: "details", sender: self.characters[indexPath.row])
        }
        
    }
    
    func searchCharacterByName(name: String, offset: Int, completionHandler: @escaping (_ result: Bool) -> Void) {
        
        RappleActivityIndicatorView.startAnimating()
        
        let apiClient = ApiMarvel(publicKey: Constants.publicKey,
                                  privateKey: Constants.privateKey,
                                  url: "characters")
        
        apiClient.send(GetCharacters(nameStartsWith: name, limit: Constants.limitPaginationSearch, offset: offset)) { response in

            switch response {
            case .success(let dataContainer):
                self.offset = dataContainer.limit + dataContainer.offset
                self.total = dataContainer.total
                
                if (dataContainer.results.isEmpty) {
                    let alertController = UIAlertController(title: "Information", message: "No results found for \(name).", preferredStyle: UIAlertController.Style.alert)
                    let ok = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                        self.search.becomeFirstResponder()
                    }
                    alertController.addAction(ok)
                    self.present(alertController, animated: true, completion: nil)
                }
                
                for character in dataContainer.results {
                    self.characters.append(["id" : character.id, "thumbnail" : (character.thumbnail?.url.absoluteString)!, "name" : character.name!, "description" : character.description!])

                }
                completionHandler(true)

            case .failure(let error):
                print(error)
                completionHandler(false)
            }
            
        }

    }
    
    /*// MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
    }*/
    

}
