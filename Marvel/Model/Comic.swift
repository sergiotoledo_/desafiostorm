//
//  Comic.swift
//  Marvel
//
//  Created by Sérgio Toledo on 10/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import Foundation

public struct Comic: Decodable {
    public let id: Int
    public let title: String?
    public let issueNumber: Double?
    public let description: String?
    public let pageCount: Int?
    public let thumbnail: Image?
}
