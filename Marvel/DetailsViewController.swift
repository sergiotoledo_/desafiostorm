//
//  DetailsViewController.swift
//  Marvel
//
//  Created by Sérgio Toledo on 10/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import UIKit
import Foundation
import RappleProgressHUD

class DetailsViewController: UIViewController {

    var details : [String:Any] = [String:Any]()
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var characterName: UILabel!
    @IBOutlet weak var characterDescription: UILabel!
    
    @IBAction func showComics(_ sender: Any) {
        
        self.performSegue(withIdentifier: "comics", sender: self.details)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        RappleActivityIndicatorView.startAnimating()
        
        characterDescription.numberOfLines = 0
        characterDescription.sizeToFit()
        
        //characterImage.load(url: URL(string: (details["thumbnail"] as! String))!)
        characterImage.loadImageUsingCache(withUrl: (details["thumbnail"] as! String))
        characterName.text = (details["name"] as! String)
        characterDescription.text = (details["description"] as! String)
        
        RappleActivityIndicatorView.stopAnimation()
        
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "comics" {
            let comicsViewController = segue.destination as! ComicsViewController
            let details = sender as? [String:Any]
            comicsViewController.details = details!
        }
        
    }
    

}
