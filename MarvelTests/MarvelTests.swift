//
//  MarvelTests.swift
//  MarvelTests
//
//  Created by Sérgio Toledo on 06/12/18.
//  Copyright © 2018 Sérgio Toledo. All rights reserved.
//

import XCTest
@testable import Marvel

class MarvelTests: XCTestCase {
    
    var api: ApiMarvel!
    var search: SearchViewController!
    var comics: ComicsViewController!
    var characters: HomeViewController!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        api = ApiMarvel(publicKey: Constants.publicKey,
                      privateKey: Constants.privateKey,
                      url: "characters")
        
        search = SearchViewController()
        comics = ComicsViewController()
        characters = HomeViewController()
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        api = nil
        search = nil
        comics = nil
        characters = nil
    }
    
    func testApiMarvelGetCharacters() {
        
        let expectation = self.expectation(description: "Loading")
        var result: Bool = false
        
        api.send(GetCharacters()){ response in

            switch response {
            case .success:
                result = true
            case .failure:
                result = false
            }
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(result == true, "Resultado da consulta vazio")
        
    }
    
    func testApiMarvelGetCharactersByName(){
        
        let expectation = self.expectation(description: "Search")
        var result: Bool = false
        
        api.send(GetCharacters(nameStartsWith: "A-Bomb (HAS)")){ response in
            
            switch response {
            case .success:
                result = true
            case .failure:
                result = false
            }
            expectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(result == true, "Resultado da busca vazio")
        
    }
    
    func testLoadCharactersHome(){
        
        let expectation = self.expectation(description: "Loading")
        
        let termSearch: String = "3-D Man"
        
        characters.loadCharacters(offset: 0, completionHandler: { result in
            
            XCTAssert(result == true, "Resultado do consumo falhou")
            
            XCTAssert(self.characters.characters.count > 0, "Não encontrou um resultado")
            
            let resultSearch = (self.characters.characters[0]["name"] as! String)
            
            XCTAssert(resultSearch.contains(termSearch), "Não encontrou um resultado")
            
            expectation.fulfill()
            
        })
        
        self.waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error, "Algo deu errado depois de 5 segundos");
        }
        
    }
    
    func testSearchCharacterByName(){
        
        let expectation = self.expectation(description: "Search")
        
        let termSearch: String = "3-D"
        
        search.searchCharacterByName(name: termSearch, offset: 0, completionHandler: { result in
            
            XCTAssert(result == true, "Resultado do consumo falhou")
            
            XCTAssert(self.search.characters.count > 0, "Não encontrou um resultado")
            
            let resultSearch = (self.search.characters[0]["name"] as! String)
            
            XCTAssert(resultSearch.contains(termSearch), "Não encontrou um resultado")
            
            expectation.fulfill()
            
        })
        
        self.waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error, "Algo deu errado depois de 5 segundos");
        }
        
    }

    
    func testloadComics(){
        
        let expectation = self.expectation(description: "Loading")
        
        let idCharacter: Int = 1017100
        
        let firstTitleSearch: String = "Hulk (2008) #55"
        
        comics.loadComics(id: idCharacter, offset: 0, completionHandler: { result in
            
            XCTAssert(result == true, "Resultado do consumo falhou")
            
            XCTAssert(self.comics.comicsList.count > 0, "Não encontrou um resultado")
            
            let resultSearch = (self.comics.comicsList[0]["title"] as! String)

            XCTAssert(resultSearch.contains(firstTitleSearch), "Não encontrou um resultado")
            
            expectation.fulfill()
            
        })
        
        self.waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error, "Algo deu errado depois de 5 segundos");
        }
        
    }
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
